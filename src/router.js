import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/restaurant',
      name: 'list',
      component: () => import('./views/List.vue')
    },
    {
      path: '/restaurant/:id',
      name: 'restaurant',
      component: () => import('./views/Restaurant.vue')
    },
    {
      path: '/payment',
      name: 'payment',
      component: () => import('./views/Payment.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
        meta: {
          notAuth: true
        }
    },
    {
      path: '/member/info',
      name: 'Info',
      component: () => import('./views/member/Info.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/member/orderlist',
      name: 'orderlist',
      component: () => import('./views/member/Orderlist.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/member/orderlist/:id',
      name: 'orderdetail',
      component: () => import('./views/member/Orderdetail.vue'),
      meta: {
        requiresAuth: true
      }
    }
  ],
  
  scrollBehavior() {
    return {
      x: 0,
      y: 0
    }
  }
})

export default router